import 'package:flutter/material.dart';
import 'data.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => MyHomePage(),
        '/detail': (context) => DetailPage()
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<List<DetailArguments>> futureData;

  Future<List<DetailArguments>> _getBooks() async {
    var data = await http
        .get("https://www.googleapis.com/books/v1/volumes?q={JoanneKRowling}%27");

    if (data.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      //return Album.fromJson(json.decode(response.body));

      var jsonData = json.decode(data.body);

      //print(jsonData);

      List<DetailArguments> books = [];

      for (var j in jsonData["items"]) {
        var volumeInfo = j["volumeInfo"];

        DetailArguments book = DetailArguments(
            volumeInfo["title"],
            volumeInfo["authors"][0],
            volumeInfo["publisher"],
            volumeInfo["publishedDate"]);
        books.add(book);
      }

      return books;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }

  @override
  void initState() {
    super.initState();
    futureData = _getBooks();
  }

  Future<List<DetailArguments>> refreshList(){
    setState(() {
      futureData = _getBooks();
    });

    return futureData;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Books"),
      ),
      body: FutureBuilder(
        future: futureData,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasData) {
            return RefreshIndicator(
              onRefresh: refreshList,
              child: ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int i) {
                  return ListTile(
                    title: Text(snapshot.data[i].title),
                    onTap: () {
                      Navigator.pushNamed(context, '/detail',
                          arguments: snapshot.data[i]);
                    },
                  );
                },
              ),
            );
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          return Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class DetailArguments {
  final String title;
  final String author;
  final String publisher;
  final String publishedDate;

  DetailArguments(this.title, this.author, this.publisher, this.publishedDate);
}

class DetailPage extends StatelessWidget {
  static const routeName = '/detail';

  @override
  Widget build(BuildContext context) {
    final DetailArguments args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text("Detail-Page"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text(args.title),
            Text(args.author),
            Text(args.publisher),
            Text(args.publishedDate)
          ],
        ),
      ),
    );
  }
}
